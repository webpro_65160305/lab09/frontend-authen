import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import { useLoadingStore } from './loading'
import productService from '@/services/product'

export const usePosStore = defineStore('pos', () => {
  const products1 = ref<Product[]>([])
  const products2 = ref<Product[]>([])
  const products3 = ref<Product[]>([])
  const loadingStore = useLoadingStore()
  async function getProducts() {
    try {
      loadingStore.doLoad()
      let res = await productService.getProductsByType(1)
      products1.value = res.data
      res = await productService.getProductsByType(2)
      products1.value = res.data
      res = await productService.getProductsByType(3)
      products1.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  return { products1, products2, products3,getProducts}
})
